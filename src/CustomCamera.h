
#include "Ogre.h"
#include "OgreCameraMan.h"
#include "OgreApplicationContext.h"
#include "OgreApplicationContext.h"
#include "OgreRTShaderSystem.h"
#include <OgreRenderWindow.h>
#include <exception>

using namespace Ogre;
using namespace OgreBites;

class CustomCamera : ApplicationContext
{
private:

	SceneNode* camSceneNode;
	Camera* camera;
	
	Viewport* viewport;

public:

	CustomCamera();
	~CustomCamera();

	void setupCamera(SceneManager* sceneManager);
	void linkCamera(Viewport* &vp);

	Camera* getCamera();

};

