#pragma once

#include "Ogre.h"
#include "OgreApplicationContext.h"
#include "OgreInput.h"
#include "OgreRTShaderSystem.h"
#include "OgreApplicationContext.h"
#include "OgreCameraMan.h"

#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

#include <iostream>


using namespace Ogre;
using namespace OgreBites;
using namespace std;

class Player
{
private:

	float x, y, z;

	Entity* playerEntity;
	SceneNode* playerSceneNode;
	
	AnimationState* playerAnimationState;  /* NINJA ANIMATIONS: Spin, Sidekick,  Stealth, JumpNoHeight, Jump, 
											  Idle1, Idle2, Idle3, Attack1, Attack2, Attack3, 
										      Backflip, Death1, Death2, Kick, Climb, Block, Crouch */


	
	
	// colision/physics
	Vector3 meshBoundingBox;
	btCollisionShape* colShape; // shape of collision e.g. box or capsule
	btRigidBody* playerRb;
	btDiscreteDynamicsWorld* dynamicsWorld;

	btTransform trans;
	Vector3* pos;


	// INVESTIGATE MASS mayube use btScalar mass; then define in constructor
	btScalar mass;


	btScalar linearDamping; 
	btScalar angularDamping;



	// controls
	bool wDown;
	bool sDown;
	bool aDown;
	bool dDown;
	

	float forwardSpeed;
	float turnSpeed;


public:
	Player();
	Player(SceneManager* sceneManager, float x, float y, float z);
	~Player();

	void createEntity(SceneManager* sceneManager, String meshToLoad);
	void attachNode(SceneNode * parent);


	void setPosition(float x, float y, float z);
	void setRotation(Vector3 axis, Radian rads);
	void setScale(float x, float y, float z);

	AnimationState getdaAnimationState();

	void createBoundingBox();
	void displayBoundingBox(bool isTrue);

	
	
	Vector3* getposition();
	

	void animationEnabled(bool setEnabled);
	

	void update();

	void setPlayerAnimation(string animationState, bool isLoop);
	AnimationState* getAnimationState();

	void createRigidBody();
	void addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes);
	void addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld);


	// could just have the methods return bools maybe?
	void wKey(bool isPressed);
	void sKey(bool isPressed);
	void aKey(bool isPressed);
	void dKey(bool isPressed);

	// Probably a better way to do this, got two sets of bool methods and one set of bool variables
	bool getWDown();
	bool getSDown();
	bool getADown();
	bool getDDown();


	bool isMoving();

	void activateRigidBody();

	void forward();
	void backward();
	void turnLeft();
	void turnRight();
	
	
	void spinLeft();
	void spinRight();

	void setLinearVelocity(btVector3 velocity);
	void setAngularVelocity(btVector3 velocity);

	void translatePlayer();
	
};

