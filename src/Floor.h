#pragma once

#include "CustomEntity.h"

class Floor : public CustomEntity
{

private:

	MeshManager* meshManager;






public:

	Floor();
	Floor(SceneManager* sceneManager);
	~Floor();

	void createPlane(SceneManager* sceneManager);

	// void createEntity(SceneManager * scnMgr, String meshToLoad) const override;
	
};