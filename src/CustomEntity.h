

#include "Ogre.h"

#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

#include <iostream>

using namespace Ogre;

 class CustomEntity
{
protected:


	

	SceneNode* sceneNode;
	Entity* entity;

	Vector3 meshBoundingBox;

	float _mass = 1.0f;

	// possible animation integration
	AnimationState* animationState;

	// possible physics integration
	btCollisionShape* colShape;  /**< Collision shape, describes the collision boundary */
	btRigidBody* rb;           /**< Rigid Body */
	btDiscreteDynamicsWorld* dynamicsWorld;
	btScalar linearDamping;
	btScalar angularDamping;

	


public:

	// perameterless constructor will produce a null object - may be useful down the line
	CustomEntity();

	// perameter based constructor will be useful for quickly creating objects
	CustomEntity(SceneManager* sceneManager, String meshToload);
	~CustomEntity();

	virtual void setCastShadows(bool castShadows);

	virtual void setPosition(float x, float y, float z);


	virtual void setRotation(float w, float x, float y, float z);
	virtual void setRotation(Vector3 axis, Radian rads);
	virtual void setScale(float x, float y, float z);

	virtual void createEntity(SceneManager * scnMgr, String meshToLoad);

	virtual void attachToNode(SceneNode * parent);
	virtual void setMaterialName(String filePath);
	virtual void createBoundingBox();


	virtual void displayBoundingBox(bool isTrue);


	virtual void setMass(float mass);
	virtual float getMass();

	virtual	void createRigidBody();

	virtual void addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &colishionShapes);
	virtual void addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld);

	virtual void update();
	virtual Vector3 getPosition();
	virtual Quaternion getOrientation();


	AnimationState* getAnimationState();



};