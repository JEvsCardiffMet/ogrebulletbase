#pragma once

#include "Ogre.h"
#include "btBulletDynamicsCommon.h"
#include "btBulletCollisionCommon.h"

#include <iostream>
using namespace Ogre;

class Enemy 
{
private:

	Entity* enemyEntity;
	SceneNode* enemySceneNode;
	


	AnimationState* animationState; // Animations: Die Idle Shoot Slump Walk

	// colision/physics
	Vector3 meshBoundingBox;
	btCollisionShape* colShape; // shape of collision e.g. box or capsule
	btRigidBody* enemyRb;
	btDiscreteDynamicsWorld* dynamicsWorld;
	
	btTransform trans;

	Real distance;
	Real speed;
	//Real move;

	Quaternion quat;

	Vector3 direction;
	btVector3 btDirection;

	std::deque<Vector3> destinationList;
	Vector3 destination;
	
	btScalar linearDamping;
	btScalar angularDamping;




public:

	Enemy();

	~Enemy();

	void createEntity(SceneManager* sceneManager, String meshToLoad);
	void attachNode(SceneNode * parent);


	void setPosition(float x, float y, float z);
	void setRotation(Vector3 axis, Radian rads);
	void setScale(float x, float y, float z);

	void addVectorToDeque(Vector3 position);


	void createBoundingBox();
	void displayBoundingBox(bool isTrue);

	void createRigidBody();
	void addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes);
	void addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld);
	void update();
	void setAnimationState(String animation, bool loop);

	AnimationState* getAnimationState();
	
	void walking(float timeSinceLastFrame);
	bool nextDestination();

};