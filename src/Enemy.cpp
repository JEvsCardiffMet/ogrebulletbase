#include "Enemy.h"

Enemy::Enemy()
{
	enemyEntity = nullptr;
	enemySceneNode = nullptr;

	linearDamping = 0.2f;
	angularDamping = 0.8f;
	 
	distance = 0;
	speed = 100;
	direction = Vector3::ZERO;
	destination = Vector3::ZERO;
	
	

	
}

Enemy::~Enemy()
{
}


void Enemy::createEntity(SceneManager * sceneManager, String meshToLoad)
{
	enemyEntity = sceneManager->createEntity(meshToLoad);
	enemyEntity->setCastShadows(false);
}




// again if I use the basic constructor, then I need a method to attach that scene node to the parent RootNode
void Enemy::attachNode(SceneNode* parent)
{
	enemySceneNode = parent->createChildSceneNode();
	enemySceneNode->attachObject(enemyEntity);

}


// simple set potision for object
void Enemy::setPosition(float x, float y, float z)
{
	enemySceneNode->setPosition(x, y, z);
}

// set rotation for object
void Enemy::setRotation(Vector3 axis, Radian rads)
{
	Quaternion quat(rads, axis); // create quaternion from converting radian
	enemySceneNode->setOrientation(quat); // apply the orientation
}

// set scale for object
void Enemy::setScale(float x, float y, float z)
{
	enemySceneNode->setScale(x, y, z);
}

void Enemy::setAnimationState(String animation, bool loop)
{
	animationState = enemyEntity->getAnimationState(animation);
	animationState->setLoop(loop);
	animationState->setEnabled(true);
}

AnimationState* Enemy::getAnimationState()
{
	return animationState;
}

void Enemy::addVectorToDeque(Vector3 position)
{
	destinationList.push_back(position);
}

void Enemy::createBoundingBox()
{
	enemySceneNode->_updateBounds();
	const AxisAlignedBox& box = enemySceneNode->_getWorldAABB();
	Vector3 temp(box.getSize()); // -200 on the y sorts the ground issue but need to test cube above it


	meshBoundingBox = temp;

	enemySceneNode->showBoundingBox(true);
}

void Enemy::displayBoundingBox(bool isTrue)
{
}

void Enemy::createRigidBody()
{
	colShape = new btBoxShape(btVector3(meshBoundingBox.x / 4, meshBoundingBox.y / 5, meshBoundingBox.z / 4));

	// create dynamic objects
	btTransform startTransform;
	startTransform.setIdentity();
	Quaternion quat = enemySceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat.x, quat.y, quat.z, quat.w));

	Vector3 pos = enemySceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	btScalar mass(1.0f); // change maybe

	// bool to decide if rigidbody should be dynamic (only dynamic if not equal to 0)
	bool isDynamic = (mass != 0.0f);

	btVector3 localInertia(0, 0, 0);

	// if isDynamic, then calculate local inertia
	if (isDynamic)
	{
		colShape->calculateLocalInertia(mass, localInertia);
	}

	// Motionstate to syncronise with only active objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);

	// apply rigidbody to playerRb
	enemyRb = new btRigidBody(rbInfo);

	enemyRb->setDamping(linearDamping, angularDamping);

	// set pointer to this object
	enemyRb->setUserPointer((void*)this);
}

void Enemy::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*>& collisionShapes)
{
	collisionShapes.push_back(colShape);
}

void Enemy::addToDynamicsWorld(btDiscreteDynamicsWorld * dynamicsWorld)
{
	this->dynamicsWorld = dynamicsWorld;
	dynamicsWorld->addRigidBody(enemyRb);
}

void Enemy::update()
{
	
// enemyRb->getMotionState()->getWorldTransform(trans);
	  
	  //	btDirection = btVector3(destination.x, 0, destination.z);
		// btDirection.normalize();
	
	if (enemyRb && enemyRb->getMotionState())
	{

	//	trans.getOrigin().setValue(move * direction.x, 0, move * direction.z);
		
		btQuaternion orientation = trans.getRotation(); // maybe this would be direction

	enemySceneNode->setPosition(Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
	//	enemySceneNode->setOrientation(Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));

	}
}


void Enemy::walking(float timeSinceLastFrame)
{
	enemyRb->activate();
	if (direction == Vector3::ZERO)
	{
		if (nextDestination())
		{
			animationState = enemyEntity->getAnimationState("Walk");
			animationState->setLoop(true);
			animationState->setEnabled(true);
		}
	}
	else
	{
		Real move = speed * timeSinceLastFrame;
		distance -= move;
		

		if (distance <= 0)
		{
			
			enemySceneNode->setPosition(destination);
			direction = Vector3::ZERO;

			if (nextDestination())
			{
				Vector3 src = enemySceneNode->getOrientation() * Ogre::Vector3::UNIT_X;

				if ((1.0 + src.dotProduct(direction)) < 0.0001)
				{
					enemySceneNode->yaw(Ogre::Degree(180));
				}
				else
				{
					 quat = src.getRotationTo(direction);
					enemySceneNode->rotate(quat);
				}
			}
			else
			{
				animationState = enemyEntity->getAnimationState("Idle");
				animationState->setLoop(true);
				animationState->setEnabled(true);
			}
		}
		else
		{
			// will move the enemy but does not work with bullet
			enemySceneNode->translate(direction * move);

			// Attempt at physics movemnet, can't get it to work:
			// enemyRb->applyCentralForce(btDirection);
		}
	}
}

	

bool Enemy::nextDestination()
{
	enemyRb->clearForces();
	if (destinationList.empty()) {
		return false;
	}

	enemyRb->setLinearVelocity(btVector3(0, 0, 0));
	enemyRb->setAngularVelocity(btVector3(0, 0, 0));

	



	destinationList.push_back(destination);
		destination = destinationList.front();
		destinationList.pop_front();
		direction = destination - enemySceneNode->getPosition();

		 distance = direction.normalise();


			// custom attempt at fixing physics problem
		btDirection = btVector3(destination.x - trans.getOrigin().getX(), 0, 0);
		// btDirection = btVector3(destination.x, 0, 0);

		//	btDirection.normalize();
		
	
		std::cout << "destination: " << btDirection << std::endl;
		
		return true;

}

