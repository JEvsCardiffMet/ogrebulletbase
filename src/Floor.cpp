#include "Floor.h"


Floor::Floor()
{
}

Floor::Floor(SceneManager * sceneManager)
{

}

Floor::~Floor()
{
}

// peraphs return Plane
void Floor::createPlane(SceneManager* sceneManager)
{
	Plane plane(Vector3::UNIT_Y, 0);

	meshManager->getSingleton().createPlane("ground", RGN_DEFAULT,
		plane, 3000, 3000, 20, 20,
		true, 1, 5, 5, Vector3::UNIT_Z);
	

}

/*
void Floor::createEntity(SceneManager * scnMgr, String meshToLoad) const
{
	Plane plane(Vector3::UNIT_Y, 0);

	meshManager->getSingleton().createPlane(meshToLoad, RGN_DEFAULT,
		plane, 3000, 3000, 20, 20,
		true, 1, 5, 5, Vector3::UNIT_Z);

}
*/
