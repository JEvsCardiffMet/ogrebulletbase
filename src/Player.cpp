#include "Player.h"

// default constructor assigns null, useful for failsafe
Player::Player()
{
	playerSceneNode = nullptr;
	playerEntity = nullptr;

	colShape = nullptr;
	dynamicsWorld = nullptr;

	wDown = false;
	sDown = false;
	aDown = false;
	dDown = false;

	 forwardSpeed = 50.0f;
	 turnSpeed = 20.0f;

	linearDamping = 0.2f;
	angularDamping = 0.8f;

	mass = 1.0f;
}

// custom constructor loads the player mesh and attaches the scene node to the root scene
Player::Player(SceneManager* sceneManager, float x, float y, float z)
{
	// try and setup the mesh
	try {
		playerEntity = sceneManager->createEntity("ninja.mesh"); // create entity and assign ninja mesh from resources
		playerEntity->setCastShadows(true);

		
		playerSceneNode = sceneManager->getRootSceneNode()->createChildSceneNode(); // create and assign playerSceneNode to a child scene node

		playerSceneNode->attachObject(playerEntity); // attach ninjaPlayer entity to the player scene node
		
		

		//playerSceneNode->setPosition(x, y, z); 
		
		//forwardSpeed = 100.0f;
	}
	// catch exception if try fails
	catch (exception e) {
		e.what();
	}

	wDown = false;
	sDown = false;
	aDown = false;
	dDown = false;

	forwardSpeed = 30.0f;
	turnSpeed = 20.0f;

	linearDamping = 0.2f;
	angularDamping = 0.8f;
	mass = 1.0f;
}


Player::~Player()
{
}




// simple set potision for object
void Player::setPosition(float x, float y, float z) 
{
	playerSceneNode->setPosition(x, y, z);
}

// set rotation for object
void Player::setRotation(Vector3 axis, Radian rads)
{
	Quaternion quat(rads, axis); // create quaternion from converting radian
	playerSceneNode->setOrientation(quat); // apply the orientation
}

// set scale for object
void Player::setScale(float x, float y, float z)
{
	playerSceneNode->setScale(x, y, z);
}


// if we use the basic constructor, then we need to assign the ninja mesh through method
void Player::createEntity(SceneManager * sceneManager, String meshToLoad)
{
	playerEntity = sceneManager->createEntity(meshToLoad);
	playerEntity->setCastShadows(false);
}




// again if I use the basic constructor, then I need a method to attach that scene node to the parent RootNode
void Player::attachNode(SceneNode* parent) 
{
	playerSceneNode = parent->createChildSceneNode();
	playerSceneNode->attachObject(playerEntity);

}

void Player::setPlayerAnimation(string animationState, bool isLoop = true)
{
	playerAnimationState = playerEntity->getAnimationState(animationState); // secret sauce to "get" animation state??
	playerAnimationState->setLoop(isLoop);
	playerAnimationState->setEnabled(true);
}

Vector3* Player::getposition()
{
	pos = new Vector3(trans.getOrigin().getX(),
		trans.getOrigin().getY(), trans.getOrigin().getZ());

	return pos;
}

void Player::animationEnabled(bool setEnabled)
{

	playerAnimationState->setEnabled(setEnabled);
}

AnimationState* Player::getAnimationState()
{
	return playerAnimationState;
}

 // dont think it works
AnimationState Player::getdaAnimationState()
{
	AnimationState*  animation = playerEntity->getAnimationState(playerAnimationState->getAnimationName());

	AnimationState ani = *animation;

	return ani;
}

void Player::createBoundingBox()
{
	playerSceneNode->_updateBounds();
	const AxisAlignedBox& box = playerSceneNode->_getWorldAABB();
	Vector3 temp(box.getSize()); // -200 on the y sorts the ground issue but need to test cube above it


	meshBoundingBox = temp;

	playerSceneNode->showBoundingBox(true);
}

void Player::displayBoundingBox(bool isTrue)
{
	playerSceneNode->showBoundingBox(isTrue);
}

void Player::createRigidBody()
{
	colShape = new btBoxShape(btVector3(meshBoundingBox.x /2, meshBoundingBox.y / 5, meshBoundingBox.z / 2));

	// create dynamic objects
	btTransform startTransform;
	startTransform.setIdentity();
	Quaternion quat = playerSceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat.x, quat.y, quat.z, quat.w));

	Vector3 pos = playerSceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	btScalar mass(mass); 

	// bool to decide if rigidbody should be dynamic (only dynamic if not equal to 0)
	bool isDynamic = (mass != 0.0f);

	btVector3 localInertia(0, 0, 0);

	// if isDynamic, then calculate local inertia
	if (isDynamic) 
	{
		colShape->calculateLocalInertia(mass, localInertia);
	}

	// Motionstate to syncronise with only active objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);
	
	// apply rigidbody to playerRb
	playerRb = new btRigidBody(rbInfo);

	playerRb->setDamping(linearDamping, angularDamping);

	// set pointer to this object
	playerRb->setUserPointer((void*)this);
}

// add to collision shapes
void Player::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes)
{
	collisionShapes.push_back(colShape);
}

// add rigidbody to bullet's dynamicsWorld
void Player::addToDynamicsWorld(btDiscreteDynamicsWorld* dynamicsWorld)
{
	this->dynamicsWorld = dynamicsWorld;
	dynamicsWorld->addRigidBody(playerRb);
}

void Player::update()
{
	

	playerRb->getMotionState()->getWorldTransform(trans);
	//  && playerRb->getMotionState()
	if (playerRb && playerRb->getMotionState())
	{
		
		
		btQuaternion orientation = trans.getRotation();

		playerSceneNode->setPosition(Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
		playerSceneNode->setOrientation(Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
	}

	
}

void Player::wKey(bool isPressed)
{
	wDown = isPressed;
}

void Player::sKey(bool isPressed)
{
	sDown = isPressed;
}

void Player::aKey(bool isPressed)
{
	aDown = isPressed;
}

void Player::dKey(bool isPressed)
{
	dDown = isPressed;
}

bool Player::getWDown()
{
	if (wDown)
		return true;
	else
		return false;
}

bool Player::getSDown()
{
	if (sDown)
		return true;
	else
		return false;
}

bool Player::getADown()
{
	if (aDown)
		return true;
	else
		return false;
}

bool Player::getDDown()
{
	if (dDown)
		return true;
	else
		return false;
}

bool Player::isMoving()
{
	if (wDown || sDown || aDown || dDown)
	{
		activateRigidBody();

		return true;
	}
	else {
		return false;
	}
}


void Player::activateRigidBody()
{
	if (playerRb->isInWorld())
	{
		playerRb->activate();
	}
}


// considered a forwards and backwards which just swaps the direction around based on perameters
void Player::forward()
{
	
	// could be if(wDown) maybe.. or maybe make bool method not sure yet I am making a lot of methods...
	

		btVector3 direction(0.0f, 0.0f, -1.0f);
		
		btVector3 push;

		btTransform trans;
		
		if (playerRb && playerRb->getMotionState())
		{
			// get the orientation of the rigid body in world space i.e. not local
			playerRb->getMotionState()->getWorldTransform(trans); // get world transform
			btQuaternion orientation = trans.getRotation(); // get rotation and set it to orientation

			

			push = quatRotate(orientation, direction);

			// activate the rigidbody, it will go to sleep if stopped movign and colliding (may need to reactivate as movements ceases eventually)
			playerRb->activate();

			// apply a central force
			playerRb->applyCentralForce(push * forwardSpeed);
			
		}
}

void Player::backward()
{
	
		btVector3 direction(0.0f, 0.0f, 1.0f); // posible direction variable to abstract
		
		btVector3 push;

		btTransform trans;


		if (playerRb && playerRb->getMotionState())
		{
			// get the orientation of the rigid body in world space i.e. not local
			playerRb->getMotionState()->getWorldTransform(trans); // get world transform
			btQuaternion orientation = trans.getRotation(); // get rotation and set it to orientation

			// now to rotate the local force into global

			push = quatRotate(orientation, direction);

			// activate the rigidbody, it will go to sleep if stopped movign and colliding (may need to reactivate as movements ceases eventually)
			playerRb->activate();

			// apply a central force
			playerRb->applyCentralForce(push * forwardSpeed);
			cout << "if(playerRb and playerRB->getmotionState()" << endl;
		}
}

void Player::turnLeft()
{
		
		btVector3 left(-1, 0.0f, 0.0f);
		btVector3 turn;

		btTransform trans;

		if (playerRb && playerRb->getMotionState())
		{
			
			playerRb->getMotionState()->getWorldTransform(trans);
			btQuaternion orientation = trans.getRotation();

		
			btVector3 front(trans.getOrigin());

		
			front += btVector3(0.0f, 0.0f, meshBoundingBox.z / 2);

			//orientated the local force into world space.
			turn = quatRotate(orientation, left);

			
			// try to use: body->activate();

			
			if (playerRb->getLinearVelocity().length() > 0.0f)
				playerRb->applyForce(turn, front * turnSpeed);
		}
	
}

void Player::turnRight()
{

		//Apply a turning force to the front of the body.
		btVector3 right(turnSpeed, 0.0f, 0.0f);
		btVector3 turn;

		btTransform trans;

		if (playerRb && playerRb->getMotionState())
		{
			//again get the orientation of the body.
			playerRb->getMotionState()->getWorldTransform(trans);
			btQuaternion orientation = trans.getRotation();

			//get the position of the body, so we can identify the
			//front and push it.
			btVector3 front(trans.getOrigin());

			//use original bounding mesh to get the front center
			front += btVector3(0.0f, 0.0f, meshBoundingBox.z / 2);

			//orientated the local force into world space.
			turn = quatRotate(orientation, right);

			//took this out, can't turn if your not moving.
			//body->activate();

			//better - only turn if we're moving.
			//not ideal, if sliding sideways will keep turning.
			if (playerRb->getLinearVelocity().length() > 0.0f)
				playerRb->applyForce(turn, front);
		}
	
}

void Player::spinLeft()
{
	
	
		btVector3 left(0.0f, 100.0f, 0.0f);
		btVector3 turn;

		btTransform trans;

		if (playerRb && playerRb->getMotionState())
		{
			
			playerRb->getMotionState()->getWorldTransform(trans);
			btQuaternion orientation = trans.getRotation();

		
			turn = quatRotate(orientation, left);

			
			playerRb->activate();

			playerRb->applyTorque(left * turnSpeed);
		}
	
}

// I think I prefer the force in front of the player, feels a bit more like how I want it
void Player::spinRight()
{

	// get direction of the turn
	btVector3 right(0.0f, -100.0f, 0.0f);
	btVector3 turn;

	btTransform trans;

	if (playerRb && playerRb->getMotionState())
	{
		
		playerRb->getMotionState()->getWorldTransform(trans); // get worldTransform for and set to trans
		btQuaternion orientation = trans.getRotation(); // get rotation


		turn = quatRotate(orientation, right);

		
		playerRb->activate(); // activate if gone to sleep (seemingly random if it works??)

		playerRb->applyTorque(right * turnSpeed); // apply the force to spin the player
	}
	
}



void Player::setLinearVelocity(btVector3 velocity)
{
	playerRb->setLinearVelocity(velocity);
	
}

void::Player::setAngularVelocity(btVector3 velocity)
{
	playerRb->setAngularVelocity(velocity);
}


void Player::translatePlayer()
{

		Vector3 direction(0.0f, 0.0f, 1.0f);
		direction.normalise();

		
		cout << "TRANSLATE" << endl;
	
}


