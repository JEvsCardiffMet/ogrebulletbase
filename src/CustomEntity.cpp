#include "CustomEntity.h"

// similar to player class, a blank perameter constructor will set nulls
CustomEntity::CustomEntity()
{
	sceneNode = nullptr;
	entity = nullptr;

	colShape = nullptr;
	dynamicsWorld = nullptr;

}

// like the player class a quicker way to assign mesh and entity  in constructro
CustomEntity::CustomEntity(SceneManager* sceneManager, String meshToLoad)
{
	entity = sceneManager->createEntity(meshToLoad);
	
	sceneNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	sceneNode->attachObject(entity);

}

CustomEntity::~CustomEntity()
{
}

void CustomEntity::setCastShadows(bool castShadows)
{
	entity->setCastShadows(castShadows);

}

void CustomEntity::setPosition(float x, float y, float z)
{
	sceneNode->setPosition(x, y, z);
	
}


// overloading methods
void CustomEntity::setRotation(float w, float x, float y, float z)
{
	Quaternion quaternion(w, x, y, z);
	sceneNode->setOrientation(quaternion);
}

void CustomEntity::setRotation(Vector3 axis, Radian rads)
{
	Quaternion quaternion(rads, axis);
	sceneNode->setOrientation(quaternion);
}

void CustomEntity::setScale(float x = 1.0f, float y = 1.0f, float z = 1.0f)
{
	sceneNode->setScale(x, y, z);
}

void CustomEntity::createEntity(SceneManager* scnMgr, String meshToLoad)
{
	entity = scnMgr->createEntity(meshToLoad);
}

void CustomEntity::attachToNode(SceneNode* parent)
{
	sceneNode = parent->createChildSceneNode();
	sceneNode->attachObject(entity);
	//sceneNode->setScale(1.0f, 1.0f, 1.0f);
	createBoundingBox();
}

void CustomEntity::setMaterialName(String filePath)
{
	entity->setMaterialName(filePath);
}

void CustomEntity::createBoundingBox()
{
	sceneNode->_updateBounds();
	const AxisAlignedBox& box = sceneNode->_getWorldAABB();
	Vector3 temp(box.getSize()); // -200 on the y sorts the ground issue but need to test cube above it


	meshBoundingBox = temp;

	// method maybe
	sceneNode->showBoundingBox(true);
}

void CustomEntity::displayBoundingBox(bool isTrue)
{
	sceneNode->showBoundingBox(isTrue);
}

void CustomEntity::setMass(float mass)
{
	_mass = mass;
	std::cout << "MASS: " << _mass << std::endl;
}

float CustomEntity::getMass()
{
	return _mass;
}

// bounding box solution for division maybe a variable
void CustomEntity::createRigidBody()
{
	colShape = new btBoxShape(btVector3(meshBoundingBox.x / 2, meshBoundingBox.y / 1.25, meshBoundingBox.z / 2));

	// create dynamic objects
	btTransform startTransform;
	startTransform.setIdentity();
	Quaternion quat = sceneNode->_getDerivedOrientation();
	startTransform.setRotation(btQuaternion(quat.x, quat.y, quat.z, quat.w));

	Vector3 pos = sceneNode->_getDerivedPosition();
	startTransform.setOrigin(btVector3(pos.x, pos.y, pos.z));

	btScalar mass(_mass);

	// bool to decide if rigidbody should be dynamic (only dynamic if not equal to 0)
	bool isDynamic = (mass != 0.0f);

	btVector3 localInertia(0, 0, 0);

	// if isDynamic, then calculate local inertia
	if (isDynamic)
	{
		colShape->calculateLocalInertia(mass, localInertia);
	}

	// Motionstate to syncronise with only active objects
	btDefaultMotionState* myMotionState = new btDefaultMotionState(startTransform);
	btRigidBody::btRigidBodyConstructionInfo rbInfo(mass, myMotionState, colShape, localInertia);

	// apply rigidbody to a newRb
	rb = new btRigidBody(rbInfo);

	rb->setDamping(linearDamping, angularDamping);

	// set pointer to this object
	rb->setUserPointer((void*)this);

}

void CustomEntity::addToCollisionShapes(btAlignedObjectArray<btCollisionShape*> &collisionShapes)
{
	collisionShapes.push_back(colShape);
}

void CustomEntity::addToDynamicsWorld(btDiscreteDynamicsWorld * dynamicsWorld)
{
	dynamicsWorld->addRigidBody(rb);
}


// possibly not needed
void CustomEntity::update()
{
	btTransform trans;

	if (rb && rb->getMotionState())
	{

		rb->getMotionState()->getWorldTransform(trans);
		btQuaternion orientation = trans.getRotation();

		sceneNode->setPosition(Vector3(trans.getOrigin().getX(), trans.getOrigin().getY(), trans.getOrigin().getZ()));
		sceneNode->setOrientation(Quaternion(orientation.getW(), orientation.getX(), orientation.getY(), orientation.getZ()));
	}
}

Vector3 CustomEntity::getPosition()
{
	Vector3 position = sceneNode->_getDerivedPosition();
	return position;
}

Quaternion CustomEntity::getOrientation()
{
	Quaternion orientation = sceneNode->_getDerivedOrientation();

	return orientation;
}

AnimationState * CustomEntity::getAnimationState()
{
	return animationState;
}
