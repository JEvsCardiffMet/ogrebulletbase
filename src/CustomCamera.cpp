#include "CustomCamera.h"

CustomCamera::CustomCamera()
{
}

CustomCamera::~CustomCamera()
{

}


// NOTE DOES NOT NEED VIEWPORT, TEMPORARY AS I WANT TO FIND SOLUITION TO UTILISE ONE IN THIS CLASS IF POSSIBLE
void CustomCamera::setupCamera(SceneManager* sceneManager)
{
	camera = sceneManager->createCamera("mainCamera");

	camera->setNearClipDistance(5);


	camSceneNode = sceneManager->getRootSceneNode()->createChildSceneNode();
	camSceneNode->setPosition(200, 300, 800);
	camSceneNode->lookAt(Vector3(0, 0, 0), Node::TransformSpace::TS_WORLD);
	camSceneNode->attachObject(camera);


	
	// errors with viewport being null
	

	
	


}

void CustomCamera::linkCamera(Viewport* &vp)
{
	vp->setBackgroundColour(ColourValue(0, 0, 0));

	//camera->setAspectRatio(Real(vp->getActualWidth()) /
	//	Real(vp->getActualHeight()));
}

Camera* CustomCamera::getCamera()
{
	return camera;
}


